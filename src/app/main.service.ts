import { Inject, Injectable, PLATFORM_ID } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { TranslateService } from '@ngx-translate/core';

@Injectable({
  providedIn: 'root'
})
export class MainService {
  config = 'https://vyinfraserver.herokuapp.com/admin';
  constructor(private http: HttpClient) { }
  getDatafromserver(): Observable<any> {
    return this.http.get(this.config + "/getAllManagers");
    // https://vyinfraserver.herokuapp.com/admin/getAllManagers
  }
  submitProperty(data): Observable<any>{  
    return this.http.post(this.config + "/submitProperty",data);
  }
  getProperty(): Observable<any>{  
    return this.http.get(this.config + "/getProperty");
  }
}  
